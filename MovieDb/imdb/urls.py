#from django.conf.urls import url, include

#from . import views

#urlpatterns = [
 #   url('', views.index, name='index'),
 #url(r'^about/$', views.AboutPageView.as_view(), name='about'),
#]


from django.conf.urls import url
from . import views
#url patterns ^(begins with)  $(endswith)
urlpatterns = [
    url(r'^$', views.Movieview, name='index'),
    url(r'addmovie$', views.addMovie, name='movieadd'),
    url(r'edit$', views.editMovie, name='edit'),
    url(r'about/$', views.selectview, name='select'),
    url(r'home$', views.Movieview, name='select'),
    url(r'addactors$', views.addActors, name='list'),
    url(r'list$', views.Movieview, name='list'),

]

