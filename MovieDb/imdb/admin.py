from django.contrib import admin
from imdb.models import Actors, Movies
# Register your models here.

admin.site.register(Actors)
admin.site.register(Movies)
