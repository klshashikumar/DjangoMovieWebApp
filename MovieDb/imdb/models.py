from django.db import models


#Actor table creation
#In Django you need not worry about db script it will automatically creates it. 
class Actors(models.Model):
    Actor_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    sex = models.CharField(max_length=50)
    dob = models.DateField()
    bio = models.CharField(max_length=500)
    def __str__(self):
        return self.name
    class Meta:
        db_table="Actors"


#Movie table Creation
class Movies(models.Model):
    movie_id = models.AutoField(primary_key=True)
    movie_name = models.CharField(max_length=50)
    year_of_release = models.IntegerField()    
    plot = models.CharField(max_length=500)
    poster =  models.ImageField(blank=True, null=True, upload_to="images/")
    cast = models.CharField(max_length=800)
    def __str__(self):
        return self.movie_name
    class Meta:
        db_table="Movies"
